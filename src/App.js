import React, {Component} from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import PokemonSkeletonLoading from './components/PokemonSkeletonLoading';
import CustomModal from "./components/CustomModal";
import Card from 'react-bootstrap/Card'
import Container from "react-bootstrap/Container";

class App extends Component {
    state = {
        pokemons: [],
        isLoading: true,
        offset: 0,
        limit: 20,
        showDetailModal: false,
        pokemonId: "",
        pokemonName: "",
        pokemonImage: "",
        pokemonTypes: [],
        pokemonStats: [],
        pokemonAbilities: [],
    };

    componentDidMount() {
        this.fetchData();
    }

    fetchData = () => {
        const URL = "https://pokeapi.co/api/v2/pokemon?offset="+this.state.offset+"&limit="+this.state.limit;

        fetch(URL, {
            method: "GET",
        }).then(response => {
            return response.json();
        }).then((data) => {
            console.log("data")
            console.log(data);
            for (let key in data.results){
                this.state.pokemons.push({
                    ...data.results[key]
                });
            }
            this.setState({
                offset: this.state.offset + this.state.limit
            });
        }).catch(() => {
            this.setState({pokemons: [this.state.pokemons]})
        });
    };

    fetchPokemon = (URL) => {
        fetch(URL, {
            method: "GET",
        }).then(response => {
            return response.json();
        }).then((data) => {
            console.log("data")
            console.log(data);
            this.setState({
                showDetailModal: true,
                pokemonId: data.id,
                pokemonName: data.name,
                pokemonImage: data.sprites.front_default,
                pokemonTypes: data.types,
                pokemonStats: data.stats,
                pokemonAbilities: data.abilities,
            });
        }).catch(() => {
            this.setState({pokemons: []})
        });
    };

    showDetailHandler = (pokemonUrl) => {
        this.fetchPokemon(pokemonUrl)
        this.setState({isLoading: false})
    };

    closeDetailModal = () => {
        this.setState({
            showDetailModal: false,
            pokemonId: "",
            pokemonName: "",
            pokemonImage: "",
            pokemonTypes: [],
            pokemonStats: [],
            pokemonAbilities: [],
            isLoading: true,
        });
    };

    renderDetail(){
        return (
            <div>
                <div className="text-center">
                    <img src={this.state.pokemonImage} alt="pokemon" style={{display: 'block', margin: '0 auto'}}/>
                    <p style={{wordWrap:'break-word', textAlign:'center', fontWeight:'light'}}>ID: {this.state.pokemonId}</p>
                    <p style={{wordWrap:'break-word', textAlign:'center', fontWeight:'bolder', fontStyle:'italic'}}>Name: {this.state.pokemonName.toLocaleUpperCase()}</p>
                </div>
                <p style={{wordWrap:'break-word'}}>Types: </p>
                <ul>
                    {this.state.pokemonTypes.map((types, typesId) => {
                        return (
                        <li key={typesId}>{types.type.name}</li>
                        )
                    })}
                </ul>
                <p style={{wordWrap:'break-word'}}>Stats: </p>
                <ul>
                    {this.state.pokemonStats.map((stats, statsId) => {
                        return (
                            <li key={statsId}>{stats.stat.name}: {stats.base_stat}</li>
                        )
                    })}
                </ul>
                <p style={{wordWrap:'break-word'}}>Abilities: </p>
                <ul>
                    {this.state.pokemonAbilities.map((abilities, abilitiesId) => {
                        return (
                            <li key={abilitiesId}>{abilities.ability.name}</li>
                        )
                    })}
                </ul>
            </div>
        )
    }

    render() {
        console.log("state")
        console.log(this.state.pokemons)
        return (
            <React.Fragment>
                <Container fluid style={{marginTop:'3rem',paddingBottom:'8em', textAlign:'center'}}>
                    <h2 className="text-center mb-4 font-weight-bold">Pokemon - Simple PokeDex</h2>
                    <InfiniteScroll
                        initialLoad={false}
                        loadMore={this.fetchData}
                        hasMore={this.state.isLoading}
                        loader={(
                            <PokemonSkeletonLoading />
                        )}
                    >
                        <CustomModal
                            show={this.state.showDetailModal}
                            title="Pokemon Detail"
                            onHide={this.closeDetailModal}
                            primaryButton="Close"
                            primaryAction={this.closeDetailModal}
                        >
                            {this.renderDetail()}
                        </CustomModal>

                        {this.state.pokemons.map(({ name, url, index }) => (
                            <Card className="shadow-lg bg-white rounded" key={`${name}${index}`}>
                                <Card.Body onClick={() => this.showDetailHandler(url)}>
                                    <h5 className="name">{name.toLocaleUpperCase()}</h5>
                                </Card.Body>
                            </Card>
                        ))}
                    </InfiniteScroll>
                </Container>
            </React.Fragment>
        )
    }
}

export default App;