import React, {Component} from 'react';
import Modal from 'react-bootstrap/Modal'

class CustomModal extends Component {
    render() {
        return(
            <React.Fragment>
                <Modal
                    show={this.props.show}
                    onHide={this.props.onHide}
                    size="md"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                >
                    <Modal.Body>
                        <h3 className="mb-4 text-center"><strong>{this.props.title}</strong></h3>
                        <div className="mx-4 mb-1">
                            {this.props.children}
                        </div>
                        <div className="text-center mb-2">
                            <button className="btn btn-danger ml-2" onClick={this.props.primaryAction}>
                                {this.props.primaryButton.toUpperCase()}
                            </button>
                        </div>
                    </Modal.Body>
                </Modal>
            </React.Fragment>
        );
    }
}

export default CustomModal;